//
//  DaftApiHelperMock.h
//  DaftApp
//
//  Created by Gabriele Palma on 07/05/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import "DaftApiHelper.h"
#import "ApiModel.h"

@interface DaftApiHelperMock : DaftApiHelper

@property NSError* returnedError;
@property SearchSaleApiModel* returnedData;

@end
