//
//  PropertyTableViewCell.h
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <UIKit/UIKit.h>

@interface PropertyTableViewCell : UITableViewCell

@property (weak) IBOutlet UILabel* propertyTypeLabel;       // Property Type i.e. site
@property (weak) IBOutlet UILabel* houseTypeLabel;          // House Type i.e. detached
@property (weak) IBOutlet UILabel* propertyAddressLabel;    // Full address
@property (weak) IBOutlet UILabel* propertyPriceLabel;      // Price
@property (weak) IBOutlet UILabel* listedDateLabel;         // Listing date

@property (weak) IBOutlet UIImageView* propertyImage;

@end
