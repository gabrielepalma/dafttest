//
//  DaftApiHelperMock.m
//  DaftApp
//
//  Created by Gabriele Palma on 07/05/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import "DaftApiHelperMock.h"

@implementation DaftApiHelperMock

- (void)performSearchSaleFetchWithSuccessBlock:(void (^)(SearchSaleApiModel *))callerSuccessBlock andFailureBlock:(void (^)(NSError *))callerFailureBlock {
    
    if(_returnedError){
        // failed call with provided error
        callerFailureBlock(_returnedError);
    }
    else if(_returnedData){
        // succesfull call returning provided data
        callerSuccessBlock(_returnedData);
    }
    else {
        // do nothing, to test what happens when waiting for an answer
    }
}

@end
