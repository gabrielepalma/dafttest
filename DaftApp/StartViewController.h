//
//  ViewController.h
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <UIKit/UIKit.h>
#import "ApiModel.h"
#import "DaftApiHelper.h"

@interface StartViewController : UIViewController

@property (weak) IBOutlet UIButton* goButton;                           // Go button to load and perform segue
@property (weak) IBOutlet UIActivityIndicatorView* loadingIndicator;    // indicator spinner when loading data
@property (weak) IBOutlet UILabel* pageLabel;                           // label to display currently selected page number
@property (weak) IBOutlet UILabel* errorLabel;                          // label to display error message
@property (weak) IBOutlet UIStepper* pageStepper;                       // stepper to change selected page
@property (strong, nonatomic) DaftApiHelper* apiHelper;                 // Api Helper, declared here for testability


@property (strong) SearchSaleApiModel* fetchedData;                     // data that has been fetched and need to be passed to next view controller

// Go button has been tapped
- (IBAction)goTapped:(id)sender;

// Stepper valueChanged
- (IBAction)valueChanged:(UIStepper *)sender;

@end

