//
//  SalesSearchTableViewControllerTests.m
//  DaftApp
//
//  Created by Gabriele Palma on 07/05/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SalesSearchTableViewController.h"
#import "JSONModel.h"
#import "PropertyTableViewCell.h"

@interface SalesSearchTableViewControllerTests : XCTestCase

@property SalesSearchTableViewController* vcUnderTest;

@end

@implementation SalesSearchTableViewControllerTests

- (void)setUp {
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _vcUnderTest = [storyboard instantiateViewControllerWithIdentifier:@"salesSearchTableViewController"];
    UIView* v = _vcUnderTest.view; v = v;
}

- (void)tearDown {
    [super tearDown];
}

- (void)testTableView {
    SearchSaleApiModel* model = [SearchSaleApiModel new];
    PropertyApiModel* prop1 = [[PropertyApiModel alloc] initWithDictionary:@{@"large_thumbnail_url":@"imageURL1",@"full_address":@"fullAddress1",@"property_type":@"propertyType1",@"house_type":@"houseType1", @"price":@10000} error:nil];
    PropertyApiModel* prop2 = [[PropertyApiModel alloc] initWithDictionary:@{@"large_thumbnail_url":@"imageURL2",@"full_address":@"fullAddress2",@"property_type":@"propertyType2",@"house_type":@"houseType2", @"price":@20000} error:nil];
    PropertyApiModel* prop3 = [[PropertyApiModel alloc] initWithDictionary:@{@"large_thumbnail_url":@"imageURL3",@"full_address":@"fullAddress3",@"property_type":@"propertyType3",@"house_type":@"houseType3", @"price":@30000} error:nil];
    model.ads = (NSMutableArray<PropertyApiModel>*)@[prop1, prop2, prop3];
    _vcUnderTest.fetchedData = model;
    
    // checking datasource
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:0], 3);
    
    // checking one cell
    PropertyTableViewCell* cell = (PropertyTableViewCell*)[_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]];
    XCTAssertEqualObjects(cell.propertyAddressLabel.text, prop2.full_address);
    NSString* formattedPrice = [NSString stringWithFormat:@"€%@", prop2.price];
    XCTAssertEqualObjects(cell.propertyPriceLabel.text, formattedPrice);
    XCTAssertEqualObjects(cell.propertyTypeLabel.text, prop2.property_type);
    XCTAssertEqualObjects(cell.houseTypeLabel.text, prop2.house_type);
}

@end
