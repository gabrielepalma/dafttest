//
//  PropertiesApiModel.m
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "ApiModel.h"

// There's nothing to do here. JSONModel will do his job.

@implementation SearchSaleApiModel

@end

@implementation PaginationApiModel

@end

@implementation PropertyApiModel

@synthesize description;

@end


