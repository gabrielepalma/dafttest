//
//  DaftApiHelper.h
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <Foundation/Foundation.h>
#import "ApiModel.h"

@interface DaftApiHelper : NSObject

@property (strong) NSMutableDictionary* queryProperties;    // parameters to be passed to the query string
@property (strong) NSString* apiUrl;                        // base url of the apis, by default it is fetched from plist
@property (strong) NSString* apiKey;                        // api key, by default it is fetched from plist

/**
Performs a search for property on sale
 @param callerSuccessBlock
Block to be called on success
 @param callerFailureBlock
Block to be called on fail
 */
- (void) performSearchSaleFetchWithSuccessBlock:(void (^)(SearchSaleApiModel*)) callerSuccessBlock
                                andFailureBlock:(void (^)(NSError *))callerFailureBlock;

/**
Set the page number of all the upcoming queries
 @param page
The page number to be set for the "page" property of "query", inside the parameters
 */
- (void) setPage:(int)page;

@end
