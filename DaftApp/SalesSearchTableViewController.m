//
//  SalesSearchTableViewController.m
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "SalesSearchTableViewController.h"
#import <UIImageView+AFNetworking.h>
#import "PropertyTableViewCell.h"
#import "ApiModel.h"

@interface SalesSearchTableViewController ()

@end

@implementation SalesSearchTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.estimatedRowHeight = 200.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedData.ads.count;
}

// We set up the property cell using our data source
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PropertyTableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"propertyCell" forIndexPath:indexPath];
    PropertyApiModel* item = [self.fetchedData.ads objectAtIndex:indexPath.row];
    cell.propertyAddressLabel.text = item.full_address;
    cell.propertyTypeLabel.text = item.property_type;
    cell.houseTypeLabel.text = item.house_type;
    cell.propertyPriceLabel.text = [NSString stringWithFormat:@"€%ld", (long)item.price.integerValue];
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:item.listing_date.integerValue];
    NSDateFormatter* formatter = [NSDateFormatter new];
    formatter.timeStyle = NSDateFormatterNoStyle;
    formatter.dateStyle = NSDateFormatterMediumStyle;
    cell.listedDateLabel.text = [formatter stringFromDate:date];
    // If we don't have a picture we use the placeholder, otherwise we load the picture using AFNetworking
    if(item.large_thumbnail_url==nil){
        cell.propertyImage.image = [UIImage imageNamed:@"placeholder"];
    }
    else {
        [cell.propertyImage setImageWithURL:[NSURL URLWithString:item.large_thumbnail_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }
    return cell;
}


@end
