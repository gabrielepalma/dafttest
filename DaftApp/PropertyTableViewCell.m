//
//  PropertyTableViewCell.m
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "PropertyTableViewCell.h"
#import <UIImageView+AFNetworking.h>

@implementation PropertyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)prepareForReuse {
    // If cell is being reused we cancel any pending download (or we might end up showing the wrong picture)
    [self.propertyImage cancelImageDownloadTask];
}

@end
