//
//  SalesSearchTableViewController.h
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <UIKit/UIKit.h>
#import "ApiModel.h"

@interface SalesSearchTableViewController : UITableViewController

@property (strong) SearchSaleApiModel* fetchedData;

@end
