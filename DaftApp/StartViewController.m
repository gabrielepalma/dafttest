//
//  ViewController.m
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "StartViewController.h"
#import "DaftApiHelper.h"
#import <AFHTTPSessionManager.h>
#import "SalesSearchTableViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController

// Lazy getter for apiHelper
- (DaftApiHelper *)apiHelper {
    if(_apiHelper == nil){
        _apiHelper = [DaftApiHelper new];
    }
    return _apiHelper;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.goButton.layer.cornerRadius = 25.0;
    self.loadingIndicator.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Go button has been tapped
- (IBAction)goTapped:(id)sender {
    // vc state is -loading-
    self.goButton.hidden = true;
    self.loadingIndicator.hidden = false;
    [self.loadingIndicator startAnimating];
    self.errorLabel.hidden = true;
    
    __weak typeof(self) weakSelf = self;
    [self.apiHelper performSearchSaleFetchWithSuccessBlock:^(SearchSaleApiModel* response) {
        typeof(self) self = weakSelf;
        // vc state is -idle-
        [self.loadingIndicator stopAnimating];
        self.goButton.hidden = false;
        self.loadingIndicator.hidden = true;
        self.errorLabel.hidden = true;
        
        // we could also directly use [self.navigationController pushViewController:animated:] which would be less verbose and save us declaring an ivar,
        // but i generally prefere to stay consistent and only use segues when i am working with Storyboards
        self.fetchedData = response;
        [self performSegueWithIdentifier:@"mainSegue" sender:self];
        
    } andFailureBlock:^(NSError * error) {
        typeof(self) self = weakSelf;
        // vc state is -idle&error-
        [self.loadingIndicator stopAnimating];
        self.goButton.hidden = false;
        self.loadingIndicator.hidden = true;
        self.errorLabel.hidden = false;
    }];
}

// Stepper has been used
- (IBAction)valueChanged:(UIStepper *)sender {
    [self.pageLabel setText:[NSString stringWithFormat:@"%d", (int)sender.value]];
    [self.apiHelper setPage:(int)self.pageStepper.value];
    
}

// Passing data to the next vc
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"mainSegue"]){
        ((SalesSearchTableViewController*)segue.destinationViewController).fetchedData = self.fetchedData;
    }
}

@end
