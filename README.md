This is the technical test for my application to the iOS position for Daft.ie

As stated in the requirements this is a simple app with two view controllers. The first view controller allows to start a query to the search_sale endpoint of Daft API while also allowing to pick a page number. The second view controller is a table view controller that display the results in customized cells. I chose to display a small set of fields from those available from the API, which includes the fields mentioned in the requirements.

The delivered project make uses of two external libraries, namely, AFNetworking for managing the network requests and JSONModel to help marshaling from JSON to OOP models. Those libraries have been included using CocoaPods.

Some unit tests have been written to test the two view controllers. 

All files and pods have already been included in the repository thus no further configuration should be necessary. Just open the workspace and run it.