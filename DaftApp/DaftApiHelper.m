//
//  DaftApiHelper.m
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "DaftApiHelper.h"
#import <AFHTTPSessionManager.h>

@implementation DaftApiHelper

- (instancetype)init {
    self = [super init];
    if (self) {
        // Fetching configuration info from plist
        self.apiUrl = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DaftBaseUrl"];
        self.apiKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"DaftApiKey"];
        
        // Initializing query parameters
        NSMutableDictionary* query = [[NSMutableDictionary alloc] initWithDictionary:@{@"perpage":@10}];
        self.queryProperties = [[NSMutableDictionary alloc] initWithDictionary:@{@"query":query}];
    }
    return self;
}

- (void) performSearchSaleFetchWithSuccessBlock:(void (^)(SearchSaleApiModel*)) callerSuccessBlock
                                andFailureBlock:(void (^)(NSError *)) callerFailureBlock {

    NSURL *baseURL = [NSURL URLWithString:self.apiUrl];
    [self.queryProperties setObject:self.apiKey forKey:@"api_key"];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:self.queryProperties options:0 error:nil];
    NSString* parameters = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [manager POST:@"search_sale" parameters:@{@"parameters":parameters} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        // Marshaling the dictionary to OOP objects using JSONModel
        NSDictionary* dict = (NSDictionary*)responseObject;
        // Skipping two layers from the result
        dict = [[dict objectForKey:@"result"] objectForKey:@"results"];
        SearchSaleApiModel* model = [[SearchSaleApiModel alloc] initWithDictionary:dict error:nil];
        callerSuccessBlock(model);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callerFailureBlock(error);
    }];
}

// Setting page
- (void) setPage:(int)page {
    NSMutableDictionary* query = [self.queryProperties objectForKey:@"query"];
    [query setObject:[NSNumber numberWithInt:page] forKey:@"page"];
}

@end
