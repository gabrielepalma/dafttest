//
//  PropertiesApiModel.h
//  DaftApp
//
//  ┌─┐┌─┐   Copyright (c) 2015
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <JSONModel/JSONModel.h>

// This protocol is required by JSONModel
@protocol PropertyApiModel
@end

// Pagination model, not used by the app
@interface PaginationApiModel : JSONModel

@property NSNumber<Optional>* total_results;
@property NSNumber<Optional>* results_per_page;
@property NSNumber<Optional>* num_pages;
@property NSNumber<Optional>* current_page;
@property NSNumber<Optional>* first_on_page;
@property NSNumber<Optional>* last_on_page;

@end

// Search sale root model
@interface SearchSaleApiModel : JSONModel

@property (strong) PaginationApiModel<Optional>* pagination;
@property (strong) NSString<Optional>* search_sentence;
@property (strong) NSArray<PropertyApiModel>* ads;

@end

// Individual property partial model (i only included what i use, rest is ignored)
@interface PropertyApiModel : JSONModel

@property (strong) NSString<Optional>* house_type;
@property (strong) NSString<Optional>* property_type;
@property (strong) NSString<Optional>* full_address;
@property (strong) NSString<Optional>* large_thumbnail_url;

@property NSNumber<Optional>* listing_date;
@property NSNumber<Optional>* price;

@end