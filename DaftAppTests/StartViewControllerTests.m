//
//  StartViewControllerTests.m
//  DaftApp
//
//  Created by Gabriele Palma on 07/05/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "JSONModel.h"
#import "StartViewController.h"
#import "DaftApiHelperMock.h"
#import <OCMock/OCMock.h>
#import <OCMock/OCMockObject.h>

@interface StartViewControllerTests : XCTestCase

@property StartViewController* vcUnderTest;

@end

@implementation StartViewControllerTests

- (void)setUp {
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _vcUnderTest = [storyboard instantiateViewControllerWithIdentifier:@"startViewController"];
    UIView* v = _vcUnderTest.view; v = v;
}

- (void)tearDown {
    [super tearDown];
}

-(void)testInitialState{
    // checking the initial state for labels and buttons is correct
    XCTAssertEqualWithAccuracy(_vcUnderTest.pageStepper.value, 1.0f, 0.1f);
    XCTAssertFalse(_vcUnderTest.goButton.hidden);
    XCTAssertTrue(_vcUnderTest.errorLabel.hidden);
    XCTAssertTrue(_vcUnderTest.loadingIndicator.hidden);
    XCTAssertEqualObjects(_vcUnderTest.pageLabel.text, @"1");
}
- (void)testSuccessful {
    SearchSaleApiModel* model = [SearchSaleApiModel new];
    PropertyApiModel* prop1 = [[PropertyApiModel alloc] initWithDictionary:@{@"large_thumbnail_url":@"imageURL1",@"full_address":@"fullAddress1",@"property_type":@"propertyType1",@"house_type":@"houseType1", @"price":@10000} error:nil];
    PropertyApiModel* prop2 = [[PropertyApiModel alloc] initWithDictionary:@{@"large_thumbnail_url":@"imageURL2",@"full_address":@"fullAddress2",@"property_type":@"propertyType2",@"house_type":@"houseType2", @"price":@20000} error:nil];
    PropertyApiModel* prop3 = [[PropertyApiModel alloc] initWithDictionary:@{@"large_thumbnail_url":@"imageURL3",@"full_address":@"fullAddress3",@"property_type":@"propertyType3",@"house_type":@"houseType3", @"price":@30000} error:nil];
    model.ads = (NSMutableArray<PropertyApiModel>*)@[prop1, prop2, prop3];
    
    DaftApiHelperMock* apiHelper = [DaftApiHelperMock new];
    _vcUnderTest.apiHelper = apiHelper;
    apiHelper.returnedData = model;
    
    id partialMock = OCMPartialMock(_vcUnderTest);
    OCMStub([partialMock performSegueWithIdentifier:[OCMArg any] sender:[OCMArg any]]);
    [partialMock goTapped:_vcUnderTest.goButton];
    
    // checking the final state for successful query is correct
    XCTAssertFalse(_vcUnderTest.goButton.hidden);
    XCTAssertTrue(_vcUnderTest.errorLabel.hidden);
    XCTAssertTrue(_vcUnderTest.loadingIndicator.hidden);
    // checking the ext viewcontroller has been properly called
    OCMVerify([partialMock performSegueWithIdentifier:[OCMArg any] sender:[OCMArg any]]);
    XCTAssertEqualObjects(_vcUnderTest.fetchedData, model);
    
}
- (void)testFailure {
    DaftApiHelperMock* apiHelper = [DaftApiHelperMock new];
    _vcUnderTest.apiHelper = apiHelper;
    apiHelper.returnedError = [NSError new];
    [_vcUnderTest goTapped:_vcUnderTest.goButton];

    // checking the final state for unsuccessful query is correct
    XCTAssertFalse(_vcUnderTest.goButton.hidden);
    XCTAssertFalse(_vcUnderTest.errorLabel.hidden);
    XCTAssertTrue(_vcUnderTest.loadingIndicator.hidden);
}
- (void)testWaiting {
    DaftApiHelperMock* apiHelper = [DaftApiHelperMock new];
    _vcUnderTest.apiHelper = apiHelper;
    [_vcUnderTest goTapped:_vcUnderTest.goButton];
    
    // checking the final state for pending query is correct
    XCTAssertTrue(_vcUnderTest.goButton.hidden);
    XCTAssertTrue(_vcUnderTest.errorLabel.hidden);
    XCTAssertFalse(_vcUnderTest.loadingIndicator.hidden);
}
- (void)testPageStepperValueChange {
    DaftApiHelperMock* apiHelper = [DaftApiHelperMock new];
    _vcUnderTest.apiHelper = apiHelper;
    _vcUnderTest.pageStepper.value = 2;
    [_vcUnderTest valueChanged:_vcUnderTest.pageStepper];
    
    // checking both the label and apiHelper dictionary has been updated
    XCTAssertEqualObjects(_vcUnderTest.pageLabel.text, @"2");
    XCTAssertTrue([[[apiHelper.queryProperties objectForKey:@"query"] objectForKey:@"page"] isEqualToNumber:@2]);
}

@end
